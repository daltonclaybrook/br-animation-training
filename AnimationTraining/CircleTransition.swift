//
//  CircleTransition.swift
//  AnimationTraining
//
//  Created by Dalton Claybrook on 7/10/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

import UIKit

class CircleTransition: NSObject, UIViewControllerAnimatedTransitioning {

    let dismissal: Bool
    
    init(dismissal: Bool) {
        self.dismissal = dismissal
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        
        return 0.7
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let viewKey = (self.dismissal) ? UITransitionContextFromViewKey : UITransitionContextToViewKey
        guard let gradientView = transitionContext.viewForKey(viewKey), toView = transitionContext.viewForKey(UITransitionContextToViewKey), containerView = transitionContext.containerView() else {
            return
        }
        
        toView.frame = containerView.bounds
        if (toView.superview == nil) {
            if (self.dismissal) {
                containerView.insertSubview(toView, belowSubview: gradientView)
            } else {
                containerView.addSubview(toView)
            }
        }
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = gradientView.layer.bounds
        gradientLayer.startPoint = CGPointMake(1.0, 1.0) // bottom right
        gradientLayer.endPoint = CGPointMake(0.0, 0.0) // top left
        
        let opaqueColor = UIColor.greenColor().CGColor
        let transparentColor = UIColor.clearColor().CGColor
        gradientLayer.colors = [opaqueColor, opaqueColor, transparentColor, transparentColor]
        gradientView.layer.mask = gradientLayer
        
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            
            gradientView.layer.mask = nil
            transitionContext.completeTransition(true)
        }
        
        let span = 0.3
        var fromLocations = [-span, -span, 0.0, 1.0+span]
        var toLocations = [-span, 1.0, 1.0+span, 1.0+span]
        
        if (dismissal) {
            let temp = fromLocations
            fromLocations = toLocations
            toLocations = temp
        }
        
        let animation = CABasicAnimation(keyPath: "locations")
        animation.duration = self.transitionDuration(transitionContext)
        animation.fromValue = fromLocations
        animation.toValue = toLocations
        gradientLayer.addAnimation(animation, forKey: nil)
        
        CATransaction.commit()
        CATransaction.performWithoutImplicitAnimations {
            gradientLayer.locations = toLocations
        }
    }
}
