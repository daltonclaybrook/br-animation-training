//
//  BasicViewController.swift
//  AnimationTraining
//
//  Created by Dalton Claybrook on 7/10/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

import UIKit

let BasicViewControllerIdentifier = "BasicViewControllerIdentifier"

class BasicViewController: UIViewController {

    @IBOutlet var leftView: UIView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    let fromColor = UIColor.cyanColor()
    let toColor = UIColor.magentaColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.leftView.alpha = 0.0
        self.leftView.backgroundColor = self.fromColor
    }
    
    @IBAction func keyframeButtonPressed(sender: UIButton) {
        
        UIView.animateKeyframesWithDuration(3.0, delay: 0.0, options: UIViewKeyframeAnimationOptions.BeginFromCurrentState, animations: { () -> Void in
            
            UIView.addKeyframeWithRelativeStartTime(0.0, relativeDuration: 1.0/3.0, animations: { () -> Void in
                self.leftView.alpha = 1.0
            })
            UIView.addKeyframeWithRelativeStartTime(1.0/3.0, relativeDuration: 1.0/3.0, animations: { () -> Void in
                self.leftView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
                self.leftView.backgroundColor = self.toColor
            })
            UIView.addKeyframeWithRelativeStartTime(2.0/3.0, relativeDuration: 1.0/3.0, animations: { () -> Void in
                self.leftView.alpha = 0.0
            })
            }, completion: { finished in
        
            self.leftView.transform = CGAffineTransformIdentity
            self.leftView.backgroundColor = self.fromColor
        })
    }
    
    @IBAction func constraintButtonPressed(sender: UIButton) {
        
        let priority: UILayoutPriority = (self.bottomConstraint.priority == 1) ? 999 : 1
        self.bottomConstraint.priority = priority
        
        UIView.animateWithDuration(0.6, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: { () -> Void in
            
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
}
