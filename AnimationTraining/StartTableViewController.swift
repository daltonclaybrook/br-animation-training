//
//  StartTableViewController.swift
//  AnimationTraining
//
//  Created by Dalton Claybrook on 7/10/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

import UIKit

let StartCellReuseID = "StartCellReuseID"

class StartTableViewController: UITableViewController {

    let destinations = [Destination(name: "UIView", identifier: BasicViewControllerIdentifier),
                        Destination(name: "CoreAnimation", identifier: CoreAnimationViewControllerIdentifier),
                        Destination(name: "Transition", identifier: TransitionViewControllerIdentifier)]

    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.destinations.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(StartCellReuseID, forIndexPath: indexPath)
        
        let destination = self.destinations[indexPath.row]
        cell.textLabel?.text = destination.name

        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let destination = self.destinations[indexPath.row]
        if let viewController = self.storyboard?.instantiateViewControllerWithIdentifier(destination.identifier) {
            viewController.title = destination.name
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
