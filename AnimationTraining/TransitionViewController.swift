//
//  TransitionViewController.swift
//  AnimationTraining
//
//  Created by Dalton Claybrook on 7/10/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

import UIKit

let TransitionViewControllerIdentifier = "TransitionViewControllerIdentifier"

class TransitionViewController: UIViewController, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.delegate = self
    }
    
    // MARK - UINavigationControllerDelegate
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if (toVC == navigationController.viewControllers.first) {
            // do not perform this transition is we are moving back to root
            return nil
        }
        return CircleTransition(dismissal: (operation == .Pop))
    }
}
