//
//  Destination.swift
//  AnimationTraining
//
//  Created by Dalton Claybrook on 7/10/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

class Destination {
    
    let name: String
    let identifier: String
    
    init(name: String, identifier: String) {
        self.name = name
        self.identifier = identifier
    }
}
