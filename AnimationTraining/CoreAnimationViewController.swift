//
//  CoreAnimationViewController.swift
//  AnimationTraining
//
//  Created by Dalton Claybrook on 7/10/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

import UIKit

let CoreAnimationViewControllerIdentifier = "CoreAnimationViewControllerIdentifier"

class CoreAnimationViewController: UIViewController {
    
    let shapeLayer = CAShapeLayer()
    var showingCircle = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.layer.addSublayer(self.shapeLayer)
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        self.setupShapeLayer()
    }
    
    // MARK: - Actions
    
    @IBAction func animateButtonPressed(sender: UIButton) {
        
        let toValue = (self.showingCircle) ? 0.0 : 1.0
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = 1.0
        animation.fromValue = (self.showingCircle) ? 1.0 : 0.0
        animation.toValue = toValue
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        self.showingCircle = !self.showingCircle
        
        self.shapeLayer.strokeEnd = CGFloat(toValue)
        self.shapeLayer.addAnimation(animation, forKey: nil)
    }

    // MARK: - Private
    
    func setupShapeLayer() {
        
        let circleSize = CGSizeMake(200, 200)
        let width = CGRectGetWidth(self.view.bounds)
        let height = CGRectGetHeight(self.view.bounds)
        let frame = CGRectMake((width-circleSize.width)/2.0, (height-circleSize.height)/2.0, circleSize.width, circleSize.height)
        
        let startAngle = CGFloat(3.0*M_PI/2.0)
        let endAngle = CGFloat(7.0*M_PI/2.0)
        
        let path = UIBezierPath(arcCenter: CGPointMake(circleSize.width/2.0, circleSize.height/2.0), radius: circleSize.width/2.0, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        self.shapeLayer.path = path.CGPath
        self.shapeLayer.frame = frame
        self.shapeLayer.fillColor = nil
        self.shapeLayer.strokeColor = UIColor.blackColor().CGColor
        self.shapeLayer.lineWidth = 4.0
        self.shapeLayer.strokeEnd = 0.0
        self.shapeLayer.lineCap = kCALineCapRound
    }
}
