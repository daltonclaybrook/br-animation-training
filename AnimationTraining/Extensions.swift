//
//  Extensions.swift
//  AnimationTraining
//
//  Created by Dalton Claybrook on 7/10/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

import UIKit

extension CATransaction {
    
    static func performWithoutImplicitAnimations(block: ()->Void) {
        
        self.begin()
        setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        block()
        self.commit()
    }
}
